var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var sleep = require('sleep-promise');
var TYPES = require('tedious').TYPES;

var kmeans = require('node-kmeans');
var tp = require('tedious-promises');
var noRuns = 1000;

// Create connection to database
var config = {
  userName: 'picboss', // update me
  password: 'q1w2E#R$', // update me
  server: 'mypicshare.database.windows.net',
  options: {
     database: 'picsharecloud',
     encrypt: true
  }
}
tp.setConnectionConfig(config); // global scope

app.use(bodyParser());

// Set up the app
app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index');
});

app.get('/tp', function(request, response) {
  response.render('pages/tp');
});

app.get('/tp1', function(request, response) {
  response.render('pages/tp1');
});

app.get('/tp2', function(request, response) {
  response.render('pages/tp2');
});

app.get('/tp3', function(request, response) {
  response.render('pages/tp3');
});

app.get('/getCourses', function(request, response) {
  console.log(await(tp.sql("SELECT s.FirstName from students s, coursetable c where c.nationalid = s.nationalid and c.courseno = @course").parameter("course", TYPES.Int, parseInt(request.body[0])).execute()));
  response.json(await(tp.sql("SELECT s.FirstName from students s, coursetable c where c.nationalid = s.nationalid and c.courseno = @course").parameter("course", TYPES.Int, parseInt(request.body[0])).execute()));
});

app.post('/getStateVoters', async function(request, response) {
  response.json(await(tp.sql("SELECT * FROM statevotingxls").execute()));
});

app.post('/getStateVotersCleaned', async function(request, response) {
  response.json(await(tp.sql("SELECT * FROM statevoting").execute()));
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

app.post('/getcourses', async function(request, response) {
  response.json(await(tp.sql("SELECT * FROM csefall").execute()));
});

async function try_kmeans() {
  var data = await(tp.sql("SELECT * FROM statevoting").execute());
  console.log(data);
  let vectors = new Array();
  for (let i = 0 ; i < data.length ; i++) {
    vectors[i] = [ data[i]['TotalPop'] , data[i]['VotePop'] , data[i]['Registered'] , data[i]['PercentReg'] , data[i]['Voted'] , data[i]['PercentVote'] ];
  }
  kmeans.clusterize(vectors, {k: 3}, (err,res) => {
    if (err) console.error(err);
    else console.log('%o',res);
  });
/*
    TotalPop: 3717,
    VotePop: 3651,
    Registered: 2526,
    PercentReg: 68,
    Voted: 2095,
    PercentVote: 57.4 
*/
}

app.post('/kmeans', async function(request, response) {
  var data = await(tp.sql("SELECT * FROM csefall").execute());
  console.log(request.body);
  var hash = {};
  for(let i = 0 ; i < data.length ; i++) {
    if(data[i]['CourseNumber'] in hash) {
      hash[data[i]['CourseNumber']] = hash[data[i]['CourseNumber']] + data[i]['MaxEnroll'];
    } else {
      hash[data[i]['CourseNumber']] = data[i]['MaxEnroll'];
    }
  }
  let vectors = new Array();
  var index = 0;
  for(var prop in hash) {
    vectors[index] = [ prop , hash[prop] ];
    index = index + 1;
  }
  //console.log(vectors);
  kmeans.clusterize(vectors, {k: parseInt(request.body[0])}, (err,res) => {
    if (err) { console.log(err); response.json({'status' : 'failure '}); }
    else response.json({'status' : 'success', 'result': res});
  });

});

/*
async function updateTime(id, iteration, time) {
  return tp.sql('INSERT INTO querytime VALUES(@id, @iteration, @time)')
    .parameter('id', TYPES.Int, id)
    .parameter('iteration', TYPES.Int, iteration)
    .parameter('time', TYPES.Numeric, time)
    .execute();
}


async function runSingleQuery(id, nums) {
  for(var iter = 1; iter <= nums; iter++) {
    var startTime = new Date().getTime(), endTime;
    var prom = tp.sql("SELECT * FROM EQUAKE WHERE LATITUDE = -65.8617")
      .execute();
    await(prom);
    endTime = new Date().getTime();
    console.log((endTime - startTime)/1000);
    await(updateTime(id, iter, endTime - startTime));
  }
  await(tp.sql("UPDATE querystatus SET stat=1 where id = @id")
    .parameter("id", TYPES.Int, id)
    .execute());
}

async function runRangeQuery(id, nums) {
  for(var iter = 1; iter <= nums; iter++) {
    var rangeLat = [-65, 86], rangeLong = [-179, 179], wsize = 12;
    var lat = Math.floor(Math.random() * (rangeLat[1] - rangeLat[0] - wsize));
    var lng = Math.floor(Math.random() * (rangeLong[1] - rangeLong[0] - wsize));
    var startTime = new Date().getTime(), endTime;
    var prom = tp.sql("SELECT id FROM EQUAKE WHERE latitude > @latmin AND latitude < @latmax AND longitude > @longmin AND longitude < @longmax")
      .parameter('latmin', TYPES.Numeric, lat)
      .parameter('latmax', TYPES.Numeric, lat + wsize)
      .parameter('longmin', TYPES.Numeric, lng)
      .parameter('longmax', TYPES.Numeric, lng + wsize)
      .execute();
    await(prom);
    endTime = new Date().getTime();
    console.log((endTime - startTime)/1000);
    await(updateTime(id, iter, endTime - startTime));
  }
  await(tp.sql("UPDATE querystatus SET stat=1 where id = @id")
    .parameter("id", TYPES.Int, id)
    .execute());
}

app.post('/getnames', async function(request, response) {
  console.log(request.body);
    var range = request.body.range, zip = request.body.zip;
    var startTime = new Date().getTime(), endTime;
    console.log(zip);
    console.log(range[0]);
    console.log(range[1]);
    var prom = tp.sql("SELECT TOP 10 zip, e.INSTNM , e.CITY, e.INSTURL from education e, uszipcodes u where u.zip > @z1 AND u.zip < @z2 AND SAT_AVG_I > @r1 AND SAT_AVG_I < @r2 AND e.city = u.city")
      .parameter('z1', TYPES.VarChar, zip[0])
      .parameter('z2', TYPES.VarChar, zip[1])
      .parameter('r1', TYPES.Int, range[0])
      .parameter('r2', TYPES.Int, range[1])
      .execute()
      .then( function(results) {
        endTime = new Date().getTime();
        response.json({'results': results, 'time': endTime - startTime});
      })
    await(prom);
});

app.post('/querystatus', async function(request, response) {
  var id = request.body[0];
  console.log(id);
  var prom = tp.sql("SELECT * FROM querystatus WHERE id=@id")
    .parameter('id', TYPES.Int, id)
    .execute()
    .then(function(results) {
      console.log(results);
      response.json({ 'result': results, 'status': 'success'});
    }).fail(function(err) {
      response.json({ 'status':  'failure' });
    });
  await(prom);
});

app.post('/querydetails', async function(request, response) {
  var id = request.body[0];
  var prom = tp.sql("SELECT * FROM querytime WHERE id=@id")
    .parameter('id', TYPES.Int, id)
    .execute()
    .then(function(results) {
      response.json({ 'result': results, 'status': 'success'});
    }).fail(function(err) {
      response.json({ 'status':  'failure' });
    });
  await(prom);
});
*/
