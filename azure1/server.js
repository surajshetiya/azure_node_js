var azure = require('azure-storage');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var formidable = require('formidable');
var path = require('path');
var fs = require('fs');
var sleep = require('sleep-promise');
var TYPES = require('tedious').TYPES;

var tp = require('tedious-promises');
var selectRE = /^SELECT/i;
var fileSvc = azure.createFileService('DefaultEndpointsProtocol=https;AccountName=pichsarecloud;AccountKey=+UTY8nIkzYrJMOy28Z/SgPod/lbGYZFsPxDdKQ52yWkZNvPiXTv9FmGrhsqvV/GgD5TACoFkj6UuJBB2Y0XzeQ==;EndpointSuffix=core.windows.net');
var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var connectAvail = false;
var sharename = 'myshare';
var hostname = 'https://pichsarecloud.file.core.windows.net/';
var sas = '?sv=2017-04-17&ss=f&srt=sco&sp=r&se=2018-02-12T09:35:19Z&st=2018-02-12T01:35:19Z&spr=https&sig=XlYzWOPlSeXNtrGUgHJfv9g%2FplOiwvb1u4APT%2B7unac%3D';
var blobSvc = azure.createBlobService('DefaultEndpointsProtocol=https;AccountName=pichsarecloud;AccountKey=+UTY8nIkzYrJMOy28Z/SgPod/lbGYZFsPxDdKQ52yWkZNvPiXTv9FmGrhsqvV/GgD5TACoFkj6UuJBB2Y0XzeQ==;EndpointSuffix=core.windows.net');

// Create connection to database
var config = {
  userName: 'picboss', // update me
  password: 'q1w2E#R$', // update me
  server: 'mypicshare.database.windows.net',
  options: {
     database: 'picsharecloud',
     encrypt: true
  }
}
tp.setConnectionConfig(config); // global scope
var connection = new Connection(config);

// Attempt to connect and execute queries if connection goes through
connection.on('connect', function(err) {
  if (err)
    console.log(err)
  else {
    connectAvail = true;
  }
});

connection.on('error', function (err) {
    if(err.message == "Connection lost - read ECONNRESET") {
      console.log('ECONNRESET-- not crashing the application');
    }
    else if (err.message == "Connection lost - read ETIMEDOUT") {
      console.log('ETIMEDOUT-- not crashing the application');
    }
});

async function queryDatabase()
{
  console.log(runquery('SELECT * FROM pics;',{}));
}

// Options: Name - Reprsents the column name
//          Value - Array of 2 entries - Data type and value
async function runquery(query, options) {
  var result = query.match(selectRE);
  var norows = -1, rows = [], errorRet = '', success = false;
  while(!connectAvail) {
    await(sleep(200));
  }
  connectAvail = false;
  console.log('Constructing query');
  request = new Request(query, function(err, rowCount, rowsobj){
    connectAvail = true;
    if(err) {
      console.log('Error occured' + err);
      errorRet = err;
    } else {
      success = true;
    }
    norows = rowCount;
    console.log(rowCount + ' row(s) returned');
  });
  
  request.on('row', function(columns) {
    console.log('Columns' + JSON.stringify(columns));
    var tuple = {};
    columns.forEach(function(column) { 
      tuple[column.metadata.colName] = column.value;
    });
    rows.push(tuple);
  });

  console.log(options);
  for (const [key, value] of Object.entries(options)) {
    request.addParameter(key, value[0], value[1]);
  }
  connection.execSql(request); 

  if(result) {
    while(norows != rows.length && errorRet.length == 0) {
      console.log('Row details : ' + norows + ' ' + rows.length);
      await(sleep(200));
    }
    if(!success) {
      console.log('Throwing error ' + errorRet);
      throw 'Failed with error ' + errorRet;
    }
    console.log(rows);
    return rows;
  }
  while(errorRet.length == '' && !success) {
    await(sleep(200));
  }
  if(!success) {
    console.log('Throwing error ' + errorRet);
    throw 'Failed with error ' + errorRet;
  }
}

async function query() {
  runquery('INSERT INTO pics VALUES(@name, GETDATE(), @title, @likes, @liked_people, @file_name);', {
    name: [TYPES.VarChar, 'JAMIE GENIUS'],
    title: [TYPES.VarChar, 'thermometer'],
    likes: [TYPES.Int, 0],
    liked_people: [TYPES.Int, 0],
    file_name: [TYPES.VarChar, 'compost_thermometer.img']
  });
}

function createContainer(container) {
  return new Promise(function(resolve, reject) {
    blobSvc.createContainerIfNotExists(container, {publicAccessLevel : 'blob'}, function(error, result, response){
      console.log('In create container');
      if(!error){
        // Container exists and is private
        if(result)
          console.log('Created container');
        else
          console.log('Container existed');
        resolve();
      } else {
        console.log('In failure path/reject' + error);
        reject();
      }
    });
  });
}

function uploadFileBlob(container, fileName, filePath) {
  return new Promise( function(resolve, reject) {
    blobSvc.createBlockBlobFromLocalFile(container, fileName, filePath, function(error, result, response){
      console.log('In create file');
      if (!error) {
        // file uploaded
        console.log('File created');
        resolve();
      } else {
        console.log('In failure path/reject' + error);
        reject();
      }
    });
  });
}

function getBlobUrl(container, filename, sas, primary) {
  return blobSvc.getUrl(container, filename, sas, primary);
}

function getFileMetadataBlob(container, file) {
  return new Promise(function(resolve, reject) {
    blobSvc.getBlobProperties(container, file, function(error, result) {
      console.log('In getMetadata');
      if (!error) {
        resolve(result);
      } else {
        console.log('In failure path/reject' + error);
        reject();
      }
    });
  });
}

function createShare(share) {
  return new Promise(function(resolve, reject) {
    fileSvc.createShareIfNotExists(share, function(error, result, response){
      console.log('In create share');
      if(!error){
        // Container exists and is private
        if(result)
          console.log('Created share');
        else
          console.log('Share existed');
        resolve();
      } else {
        console.log('In failure path/reject' + error);
        reject();
      }
    });
  });
}

// FileService.prototype.getDirectoryProperties = function (share, directory, optionsOrCallback, callback) - azure-storage-node/blob/master/lib/services/file/fileservice.core.js
// FileService.prototype.getDirectoryMetadata = function (share, directory, optionsOrCallback, callback)
// FileService.prototype.listFilesAndDirectoriesSegmented = function (share, directory, currentToken, optionsOrCallback, callback)
// FileService.prototype.listFilesAndDirectoriesSegmentedWithPrefix = function (share, directory, prefix, currentToken, optionsOrCallback, callback)
// FileService.prototype.getFileMetadata = function (share, directory, file, optionsOrCallback, callback)
// FileService.prototype.getFileProperties = function (share, directory, file, optionsOrCallback, callback) - Seems to be better
// FileService.prototype.doesFileExist = function (share, directory, file, optionsOrCallback, callback)
// FileService.prototype.deleteFile = function (share, directory, file, optionsOrCallback, callback)
// FileService.prototype.deleteFileIfExists = function (share, directory, file, optionsOrCallback, callback)
// FileService.prototype.getFileToText = function (share, directory, file, optionsOrCallback, callback)
// FileService.prototype.startCopyFile = function (sourceUri, targetShare, targetDirectory, targetFile, optionsOrCallback, callback)
// FileService.prototype.getServiceProperties = function (optionsOrCallback, callback)

function getFileMetadata(share, dirName, file) {
  return new Promise(function(resolve, reject) {
    fileSvc.getFileProperties(share, dirName, file, function(error, result) {
      console.log('In getMetadata');
      if (!error) {
        resolve(result);
      } else {
        console.log('In failure path/reject' + error);
        reject();
      }
    });
  });
}

function createDirectory(share, dirName) {
  return new Promise(function(resolve, reject) {
      fileSvc.createDirectoryIfNotExists(share, dirName, function(error, result, response) {
      console.log('In create directory');
      if (!error) {
        if(result)
          console.log('Created directory');
        else
          console.log('Directory existed');
        resolve();
      } else {
        console.log('In failure path/reject' + error);
        reject();
      }
    });
  });
}

function uploadFile(share, dirName, fileName, filePath) {
  return new Promise( function(resolve, reject) {
    fileSvc.createFileFromLocalFile(share, dirName, fileName, filePath, function(error, result, response) {
      console.log('In create file');
      if (!error) {
        // file uploaded
        console.log('File created');
        resolve();
      } else {
        console.log('In failure path/reject' + error);
        reject();
      }
    });
  });
} 

app.use(bodyParser());

// Set up the app
app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index');
});

app.post('/list', async function(req, res) {
  var username = req.body[0];
  var result = await(runquery('SELECT * FROM pics WHERE username = @name;',{name: [TYPES.VarChar, username]}));
  for(var index = 0; index < result.length; index++) {
    // result[index].url =  fileSvc.getUrl(sharename, result[index].username, result[index].file_name, sas, hostname);
    result[index].url =  blobSvc.getUrl(result[index].username, result[index].file_name, null, hostname);
  }
  res.json(result);
});

app.post('/likes', async function(req, res) {
  var trans, obj = req.body, trLikes, trPeople;
  // create the transaction from the a tp instance 
  var prom = tp.beginTransaction()
    .then(function(newTransaction) {
      // remember the transaction, you'll need it later 
      trans = newTransaction;
      // use the transaction like a normal tp instance 
      // ('return' chains the promises) 
      return trans.sql('SELECT likes, liked_people FROM pics WHERE username=@name AND title=@title')
        .parameter('name', TYPES.VarChar,  obj.name)
        .parameter('title', TYPES.VarChar, obj.title)
        .execute();
    })
    .then(function(testResult) {
      trLikes = parseInt(testResult[0].likes);
      trPeople = parseInt(testResult[0].liked_people);
      console.log('Likes ' + (trLikes + obj.likes));
      console.log('Liked_people ' + (trPeople + 1));
      return trans.sql('UPDATE pics SET likes=@likes, liked_people=@people WHERE username=@name AND title=@title')
        .parameter('name', TYPES.VarChar,  obj.name)
        .parameter('title', TYPES.VarChar, obj.title)
        .parameter('likes', TYPES.Int, trLikes + obj.likes)
        .parameter('people', TYPES.Int, trPeople + 1)
        .execute();
    })
    .then(function(testResult) {
      res.json([ trLikes + obj.likes, trPeople + 1 ]);
      return trans.commitTransaction();
    })
    .fail(function(err) {
      // rollback on failures 
      console.log('Error occured in transaction ' + err);
      res.status(400);
      return trans.rollbackTransaction();
    });
  await(prom);
});

app.post('/upload', async function(req, res){
  // create an incoming form object
  var form = new formidable.IncomingForm(), title = '', author = '';
  var tempdir = path.join('.', 'temp');
  var filesizeerr = false;

  // specify that we want to allow the user to upload multiple files in a single request
  form.multiples = false;

  // store all uploads in the /uploads directory
  form.uploadDir = tempdir;

  form.on('field', function(name, value) {
    console.log(name + ':' + value);
    if(name == 'title') {
      title = value;
    } if(name == 'author') {
      author = value;
    }
  });

  // Define all the handlers for the file operations
  // every time a file has been uploaded successfully,
  // rename it to it's orignal name and move to the Dropbox directory
  form.on('file',async function(field, file) {
    console.log('Title' + title);
    console.log('Author' + author);
    var tempfile = path.join(tempdir, file.name);
    fs.renameSync(file.path, tempfile);
    var stats = fs.lstatSync(tempfile);
    // await(createDirectory(sharename, author));
    await(createContainer(author.toLowerCase()));
    runquery('INSERT INTO pics VALUES(@name, GETDATE(), @title, @likes, @liked_people, @file_name);', {
      name: [TYPES.VarChar, author.toString()],
      title: [TYPES.VarChar, title.toString()],
      likes: [TYPES.Int, 0], 
      liked_people: [TYPES.Int, 0], 
      file_name: [TYPES.VarChar, file.name.toString()]
    }); 
    // await(uploadFile(sharename, author, file.name, tempfile));
    await(uploadFileBlob(author.toLowerCase(), file.name, tempfile));
  });

  form.on('error', function(err) {
    console.log('An error has occured: \n' + err);
    res.status(406);
  });

  form.on('end', function() {
    if(filesizeerr)
      res.status(406);
    else
      res.end('success');
  });

  // parse the incoming request containing the form data
  form.parse(req);
});

createShare(sharename).then( () => {
  app.listen(app.get('port'), function() {
    console.log('Node app is running on port', app.get('port'));
  });
});
