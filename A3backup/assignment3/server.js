var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var sleep = require('sleep-promise');
var TYPES = require('tedious').TYPES;
var redis = require("redis");
var client = redis.createClient(6380,'redisa3.redis.cache.windows.net', {auth_pass: 'GZ8HgDwhWFwm5ZJhcdi3GneW/msM2yAx7LgpEgjh/dc=', tls: {servername: 'redisa3.redis.cache.windows.net'}});
const {promisify} = require('util');
const getAsync = promisify(client.get).bind(client);

var tp = require('tedious-promises');
var noRuns = 1000;

// Create connection to database
var config = {
  userName: 'picboss', // update me
  password: 'q1w2E#R$', // update me
  server: 'mypicshare.database.windows.net',
  options: {
     database: 'picsharecloud',
     encrypt: true
  }
}
tp.setConnectionConfig(config); // global scope

app.use(bodyParser());

// Set up the app
app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index');
});

async function getNewId() {
  var trans, newid = -1, failure = false;
  var prom = tp.beginTransaction()
    .then(function(newTransaction) {
      // remember the transaction, you'll need it later 
      trans = newTransaction;

      return trans.sql('SELECT max(id) AS max from querystatus').execute();
    })
    .then(function(maxid) {
      newid = maxid[0].max + 1;
      return trans.sql('INSERT INTO querystatus VALUES(@newid, 0)')
      .parameter('newid', TYPES.Int, newid)
      .execute();
    })
    .then(function() {
      return trans.commitTransaction();
    })
    .fail(function(err) {
      // rollback on failures 
      console.log('Error occured in transaction ' + err);
      failure = true;
      return trans.rollbackTransaction();
    });
  await(prom);
  console.log("ID:" + newid + " status : " + failure );
  if(!failure)
    return newid;
  else
    return -1;
}

async function updateTime(id, iteration, time) {
  return tp.sql('INSERT INTO querytime VALUES(@id, @iteration, @time)')
    .parameter('id', TYPES.Int, id)
    .parameter('iteration', TYPES.Int, iteration)
    .parameter('time', TYPES.Numeric, time)
    .execute();
}

async function runSingleQuery(id, nums) {
  for(var iter = 1; iter <= nums; iter++) {
    var startTime = new Date().getTime(), endTime;
    var prom = tp.sql("SELECT * FROM EQUAKE WHERE LATITUDE = -65.8617")
      .execute();
    await(prom);
    endTime = new Date().getTime();
    console.log((endTime - startTime)/1000);
    await(updateTime(id, iter, endTime - startTime));
  }
  await(tp.sql("UPDATE querystatus SET stat=1 where id = @id")
    .parameter("id", TYPES.Int, id)
    .execute());
}

async function runRangeQuery(id, nums) {
  for(var iter = 1; iter <= nums; iter++) {
    var rangeLat = [-65, 86], rangeLong = [-179, 179], wsize = 12;
    var lat = Math.floor(Math.random() * (rangeLat[1] - rangeLat[0] - wsize));
    var lng = Math.floor(Math.random() * (rangeLong[1] - rangeLong[0] - wsize));
    var startTime = new Date().getTime(), endTime;
    var prom = tp.sql("SELECT id FROM EQUAKE WHERE latitude > @latmin AND latitude < @latmax AND longitude > @longmin AND longitude < @longmax")
      .parameter('latmin', TYPES.Numeric, lat)
      .parameter('latmax', TYPES.Numeric, lat + wsize)
      .parameter('longmin', TYPES.Numeric, lng)
      .parameter('longmax', TYPES.Numeric, lng + wsize)
      .execute();
    await(prom);
    endTime = new Date().getTime();
    console.log((endTime - startTime)/1000);
    await(updateTime(id, iter, endTime - startTime));
  }
  await(tp.sql("UPDATE querystatus SET stat=1 where id = @id")
    .parameter("id", TYPES.Int, id)
    .execute());
}

async function runSingleRedis(id, nums) {
  var query = "SELECT * FROM EQUAKE WHERE LATITUDE = -65.8617";
  for(var iter = 1; iter <= nums; iter++) {
    var startTime = new Date().getTime(), endTime;
    var pro = getAsync(query).then(async function(res) {
      // Result not found
      if(res === null) {
        var prom = tp.sql("SELECT * FROM EQUAKE WHERE LATITUDE = -65.8617")
          .execute();
        var result = await(prom);
        client.set(query, JSON.stringify(result));
      }
      endTime = new Date().getTime();
      console.log((endTime - startTime)/1000);
      console.log("Cache " + res != null);
    });
    await(pro);
    console.log((endTime - startTime)/1000);
    await(updateTime(id, iter, endTime - startTime));
  }
  await(tp.sql("UPDATE querystatus SET stat=1 where id = @id")
    .parameter("id", TYPES.Int, id)
    .execute());
}

async function runRangeRedis(id, nums) {
  for(var iter = 1; iter <= nums; iter++) {
    var rangeLat = [-65, 86], rangeLong = [-179, 179], wsize = 12;
    var lat = Math.floor(Math.random() * (rangeLat[1] - rangeLat[0] - wsize));
    var lng = Math.floor(Math.random() * (rangeLong[1] - rangeLong[0] - wsize));
    var query = "SELECT id FROM EQUAKE WHERE latitude > " + lat + " AND latitude < " + (lat + wsize) + " AND longtitude > " + lng + " AND longitude < " +  (lng + wsize);
    // Cache the query
    var startTime = new Date().getTime(), endTime;
    var pro = getAsync(query).then(async function(res) {
      // Result not found
      if(res === null) {
        var prom = tp.sql("SELECT id FROM EQUAKE WHERE latitude > @latmin AND latitude < @latmax AND longitude > @longmin AND longitude < @longmax")
          .parameter('latmin', TYPES.Numeric, lat)
          .parameter('latmax', TYPES.Numeric, lat + wsize)
          .parameter('longmin', TYPES.Numeric, lng)
          .parameter('longmax', TYPES.Numeric, lng + wsize)
          .execute();
        var result = await(prom);
        client.set(query, JSON.stringify(result));
      }
    });
    await(pro);
    endTime = new Date().getTime();
    console.log((endTime - startTime)/1000);
    await(updateTime(id, iter, endTime - startTime));
  }
  await(tp.sql("UPDATE querystatus SET stat=1 where id = @id")
    .parameter("id", TYPES.Int, id)
    .execute());
}

app.post('/getRangeQuery', async function(request, response) {
  var result;
  var query = "SELECT * FROM EQUAKE WHERE LATITUDE = -65.8617";
  // Cache the query
  var pro = getAsync(query).then(async function(res) {
    console.log('I am here' + typeof(res) +res);
    res = null;
    // Result not found
    if(res === null) {
      var prom = tp.sql(query)
        .execute();
      result = await(prom);
      console.log(result);
      response.json(result);
      client.set(query, JSON.stringify(result));
    } else {
      response.json(res);
    }
  });
  await(pro);
});

app.post('/querystatus', async function(request, response) {
  var id = request.body[0];
  console.log(id);
  var prom = tp.sql("SELECT * FROM querystatus WHERE id=@id")
    .parameter('id', TYPES.Int, id)
    .execute()
    .then(function(results) {
      console.log(results);
      response.json({ 'result': results, 'status': 'success'});
    }).fail(function(err) {
      response.json({ 'status':  'failure' });
    });
  await(prom);
});

app.post('/querydetails', async function(request, response) {
  var id = request.body[0];
  var prom = tp.sql("SELECT * FROM querytime WHERE id=@id")
    .parameter('id', TYPES.Int, id)
    .execute()
    .then(function(results) {
      response.json({ 'result': results, 'status': 'success'});
    }).fail(function(err) {
      response.json({ 'status':  'failure' });
    });
  await(prom);
});

app.post('/runrangeredis', async function(request, response) {
  var id = await(getNewId());
  console.log(id);
  response.json({'id': id});
  runRangeRedis(id ,noRuns);
});

app.post('/runsingleredis', async function(request, response) {
  var id = await(getNewId());
  console.log(id);
  response.json({'id': id});
  runSingleRedis(id, noRuns);
});

app.post('/runrange', async function(request, response) {
  var id = await(getNewId());
  console.log(id);
  response.json({'id': id});
  runRangeQuery(id, noRuns);
});

app.post('/runsingle', async function(request, response) {
  var id = await(getNewId());
  console.log(id);
  response.json({'id': id});
  runSingleQuery(id, noRuns);
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
